# Applications

NERSC provides [modules](./environment/#nersc-modules-environment)
for a variety of common science codes such as:

## Density functional theory

* [CPMD](cpmd/index.md)
* [VASP](vasp/index.md)
* [Quantum ESPRESSO](quantum-espresso/index.md)
* [SIESTA](siesta/index.md)
* [WIEN2k](wien2k/index.md)

## Molecular dynamics

* [AMBER](amber/index.md)
* [NAMD](namd/index.md)

## Chemistry applications

* [MOLPRO](molpro/index.md)
* [Q-Chem](qchem/index.md)

