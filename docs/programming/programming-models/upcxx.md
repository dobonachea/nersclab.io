# UPC++

[UPC++](http://upcxx.lbl.gov) is a C++ library that supports
Partitioned Global Address Space (PGAS) programming, and is designed
to interoperate smoothly and efficiently with MPI, OpenMP, CUDA and
AMTs. It leverages GASNet-EX to deliver low-overhead, fine-grained
communication, including Remote Memory Access (RMA) and Remote
Procedure Call (RPC).

## Using UPC++ at NERSC

To see what versions are available (generally the default is
recommended).

```shell
nersc$ module avail upcxx
```
