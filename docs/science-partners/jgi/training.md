# Training and Tutorials

This page contains links to materials presented at training sessions and
tutorials that have been given at JGI.

## Downloads

* [June 2018 New User Training](https://www.nersc.gov/assets/Uploads/New-User-Training-Jun2018.pptx)
    - Training session for JGI staff new to NERSC computing resources.
      Delivered by Dan Udwary on June 14, 2018.
* [Advanced Anaconda](https://www.nersc.gov/assets/Uploads/Advanced-Anaconda.pdf)
    - Training session given by Dan Udwary and Mario Melara on June 7, 2018 at
      JGI. Covers some tips and tricks for working with Anaconda environments
      and introduces the basics of packaging software for upload to the
      Anaconda Cloud.
* [How to Break NERSC](https://www.nersc.gov/assets/Uploads/How-to-Break-NERSC.pdf)
    - A talk on the roadmap for JGI computing with NERSC, and best and worst
      practices. Delivered March 22, 2018 by Dan Udwary and Tony Wildish.
* [Anaconda Tutorial](https://www.nersc.gov/assets/Uploads/Anaconda-tutorial-11-30-2017.pptx)
    - How and why you should look at the Anaconda software manager as a
      replacement for the module system. Given at JGI on November 30, 2017.
* [JGI Modules Tutorial](https://www.nersc.gov/assets/Uploads/JGI-Modules-Tutorial-2017-11-30.pdf)
    - How and why (or why not) to build modules. Given at the JGI on November
      30, 2017.
* [Slurm Workshop](https://www.nersc.gov/assets/Uploads/SLURMWorkshop-Sep2017.pptx)
    - Intro slides for the Slurm workshop conducted at JGI on September 28,
      2017.
* [Slurm Training](https://www.nersc.gov/assets/Uploads/SLURMTraining-Aug2017.pptx)
    - Slides from a training session provided by NERSC on August 24 and 31,
      2017. Covers the expected timeline for transition to Denovo, the state of
      JGI computing, Slurm, and job accounting.
* [JGI TaskFarmer Training](https://www.nersc.gov/assets/Uploads/JGI-TFtraining-Jun2017.pptx)
    - Slides for a TaskFarmer training session given to the JGI on June 28,
      2017. TaskFarmer is a simple workflow manager.
* [Burst Buffer Tutorial](https://www.nersc.gov/assets/Uploads/Burst-Buffer-tutorial.pdf)
    - Slides from the training given at the JGI on June 28th 2017. The Burst
      Buffer is a high-performance disk that can be used to accelerate jobs on
      Cori.
* [JGI Introductory Tutorial](https://www.nersc.gov/assets/Uploads/JGItutorial-Jun2017.pptx)
    - A quick guide to the computing resources available to JGI users from
      NERSC and getting started on using them. Presentation given June 8, 2017
      by Dan Udwary.
* [Advanced Gitlab](https://www.nersc.gov/assets/Uploads/Advanced-Gitlab.pdf)
    - A tutorial on best practices and advanced features of Git and Gitlab for
      managing software, working in teams, and building Docker images and
      deploying them automatically. Given at the JGI on May 31, 2017.
* [Anaconda Tutorial](https://www.nersc.gov/assets/Uploads/Anaconda-tutorial-4-26-17.pptx)
    - A training session held on April 26, 2017, instructing use of the
      Anaconda Python distribution and associated software.
* [JGI Future of Genepool](https://www.nersc.gov/assets/Uploads/JGI-Future-of-Genepool-3-17-2017.pptx)
    - A quick overview of the changes coming to Genepool given by Dan Udwary on
      March 17, 2017 and why moving bioinformatics software to containers will
      be a critical part of that change.
* [Slurm Training](https://www.nersc.gov/assets/Uploads/SLURMTraining-Oct13-2016.pptx)
    - Slides from the JGI Slurm workshop held on October 13, 2016.
* [BLAST Tutorial](https://www.nersc.gov/assets/Uploads/BLASTtutorial-dec2015.pdf)
    - A training on using BLAST optimally on the Genepool system, given by Dan
      Udwary.
* [Gitlab CI](https://www.nersc.gov/assets/Uploads/2017-02-06-Gitlab-CI.pdf)
    - A hands-on introduction to Gitlab, a continuous integration platform,
      from February 6, 2017.
* [Git + Docker Tutorial](https://www.nersc.gov/assets/Uploads/Git-+-Docker-Tutorial-Dec01-2016.pdf)
    - Slides from the basic intro to Git and Docker at the JGI given on
      December 1, 2016.
* [Docker for Reproducible Pipelines](https://www.nersc.gov/assets/Uploads/Docker-for-reproducible-pipelines.pptx)
    - A presentation given by Tony Wildish on March 17, 2017 describing NERSC's
      recommendations for converting JGI pipelines from the Genepool module
      system to more reproducible containerized pipelines that can run on
      Denovo, Cori, and the cloud.
* [Google Cloud Training](https://www.nersc.gov/assets/Uploads/Google-Cloud-Training.pptx)
    - A hands-on workshop presented at the JGI and at the JGI User Meeting in
      March 2017. Instructs the user in the use of Docker containers and
      virtual machines in the Google Cloud.
