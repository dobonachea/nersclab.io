#!/bin/bash

#SBATCH -t 25:00   
#SBATCH  --account nstaff

# Pick one of the following lines to toggle: chos or shifter or Cori
# (toggle  '#-SBATCH' vs. '#SBATCH'  )
#-SBATCH -J star-chos -p shared-chos --ntasks=1
#SBATCH -J star-shift -p shared --ntasks=1 --image=custom:pdsf-sl64-star:v3
#-SBATCH -J star-cori -p debug -N1 -t 25:00 --image=custom:pdsf-sl64-star:v7  -C haswell

echo "start-A "`hostname`" in PWD="`pwd`

startSkew=30 # (seconds), random delay for task
nsleep=$(($RANDOM % $startSkew))
echo nsleep=$nsleep
sleep $nsleep

#tasks script to be executed
job_sh=r4sTask_bfc.csh
export NUM_EVE=9
# 1 event takses 30-60 sec (or 5 minutes), 1st event takes 300-1000 seconds depending on the DB load

# fixed one data file
#dataName=st_mtd_adc_16114048_raw_2500009.daq

# OR use data from the listB,  10k events in the daq, 1 eve taks 10-15 sec
# but some daw files, e.g. st_physics_17133038_raw_4000014.daq require 40-70 sec/eve
export PATH_DAQ=/project/projectdirs/mpccc/balewski/star_daq_2016
dataList=dataListB.txt
export STAR_VER=SL17a
export BFC_String="DbV20161216,P2016a,StiCA,mtd,mtdCalib,btof,PxlHit,IstHit,SstHit,beamline3D,picoWrite,PicoVtxVpd,BEmcChkStat,-evout,CorrX,OSpaceZ2,OGridLeak3D,-hitfilt"

export LOG_PATH=/global/project/projectdirs/mpccc/balewski/tmp/logs
mkdir -p $LOG_PATH
echo write r4s logs to  $LOG_PATH

kD=${SLURM_ARRAY_TASK_ID-1}
echo pick data $kD from list $dataList
dataName=${PATH_DAQ}/`head -n $kD  $dataList | tail -n1`

# pick STAR library you want to use
export EXEC_NAME=root4star

# define permanent output dir, here it is jobID dependent
export OUT_DIR=/global/project/projectdirs/mpccc/balewski/tmp/outSTAR3/${SLURM_JOB_NAME}/${SLURM_JOB_ID}
mkdir -p ${OUT_DIR}

# prepare sandbox - it is done for you by SLURM on PDSF and must be done manualy on Cori
export WRK_DIR=$SLURM_TMP
echo aaa $SLURM_CLUSTER_NAME
if [[ $SLURM_CLUSTER_NAME != 'pdsf'* ]] ; then
    export WRK_DIR=$CSCRATCH/tmp-star/${SLURM_JOB_ID}
    echo make sandbox $WRK_DIR which will be NOT erased by Slurm
    mkdir -p  $WRK_DIR    
fi
    

# used code must be copied to the sandbox
# optional:
# it is safer to copy all code to the sandbox, so job still runs fine even if you recompile your local code 
codeDir=/global/homes/b/balewski/star-pipeline/embedPdsf1/

echo Prepare a local copy of binaries
time( cp -rpL r4sTask_bfc.csh  $WRK_DIR ; cp -rpL $codeDir/StRoot $codeDir/.sl64_gcc482/   $WRK_DIR )

echo run job in STAR_VER=$STAR_VER  WRK_DIR=$WRK_DIR
echo see vCores=$SLURM_CPUS_ON_NODE

ls -l  ${job_sh}
if [[ $SLURM_JOB_PARTITION == *"-chos" ]]
then
    echo  run-in-chos
    CHOS=sl64 chos  $WRK_DIR/${job_sh}  $dataName
else
    echo  run-in-shifter
    # minor operation allowing to jump into tcsh inside shifter image
    unset MODULE_VERSION_STACK
    unset MODULE_VERSION
    unset MODULEPATH MODULESHOME
    unset LOADEDMODULES PRGENVMODULES
    shifter   --volume=/global/project:/project   /bin/tcsh $WRK_DIR/${job_sh} $dataName
fi
echo end-A-slurm-job

# mv slurm log to final destination 
if [ -z ${SLURM_ARRAY_JOB_ID+x} ]; then 
  mv slurm-${SLURM_JOB_ID}.out $LOG_PATH
else 
  mv slurm-${SLURM_ARRAY_JOB_ID}_${SLURM_ARRAY_TASK_ID}.out $LOG_PATH
fi

