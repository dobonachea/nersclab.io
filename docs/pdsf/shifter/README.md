# Recipes for PDSF-users speciffic Shifter tasks

(see NERSC generic Shifter tutorial for explanation of bacis concepts)

Table of content:

1. how to transfer payload of a live STAR DB and convert it into
   static directory allowing to use Shifter-based local DB to serve
   the DB tables to root4star jobs on Cori

2. Example of interactive use of Shifter at PDSF and Cori, uses CHOS-SL6.4 image, CVMFS, features Atlas simulation job, runs for any PDSF user 
