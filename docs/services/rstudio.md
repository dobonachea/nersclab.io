# RStudio

RStudio provides popular open source and enterprise-ready professional software for the R statistical computing environment.
NERSC has an RStudio instance at [rstudio.nersc.gov](https://rstudio.nersc.gov).

R users may also be interested in using an R kernel on [jupyter.nersc.gov](https://jupyter.nersc.gov).
